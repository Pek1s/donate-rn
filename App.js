import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import {Input, Button} from 'react-native-elements';


export default function App() {
  const [currency_value, onChangeNumber] = React.useState(null);
  const [activeButton, setActiveButton] = React.useState(1);
  const [activeValue, setActiveValue] = React.useState();
  const [messageActive, setMessageActive] = React.useState(false);
  const [justSubmitted, setJustSubmitted] = React.useState(false);

  const handlePress = () => {
    console.log("pressed button");
  };
  const handleCustomSubmit = () => {
    if(currency_value === null) {
      return
    }
    console.log(currency_value);
    fetch("http://ec2-3-250-54-136.eu-west-1.compute.amazonaws.com:8080/event", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        destination: activeButton === 1 ? "biking journey" : "wwf",
        currency_value: currency_value,
      }),
    });
    setActiveValue();
    onChangeNumber(null);
    setJustSubmitted(true);
    setTimeout(() => {
      setJustSubmitted(false);
    }, 3000);
  };
  const handleNumberChange = (val) => {
    setActiveValue();
    onChangeNumber(val);
  };



  return (
    <View style={styles.container}>
      {!justSubmitted ? (
        <View style={styles.container}>
          <Text style={styles.text1}>Mark here how much did you donate</Text>

          <Text style={styles.text}>Select which one you want to support</Text>
          <View style={styles.textbutton}>
          <TouchableOpacity
            style={activeButton === 1 ? styles.greenButton : styles.redButton}
            onPress={() => setActiveButton(1)}
          ><Text style={styles.appButtonText}>Biking trip</Text></TouchableOpacity>
          </View>
          <View style={styles.textbutton}>
          <TouchableOpacity
            style={activeButton === 2 ? styles.greenButton : styles.redButton}
            onPress={() => setActiveButton(2)}
          ><Text style={styles.appButtonText}>WWF</Text></TouchableOpacity>
          </View>
          <Text> </Text>
          <Text  style={styles.text}>Select amount</Text>
          <TouchableOpacity
            style={activeValue === 2 ?  styles.selectValueButton : styles.nonselectValueButton}
            onPress={() => {
              setActiveValue(2);
              onChangeNumber(2);
            }}
          ><Text style={styles.appButtonText}>2€</Text></TouchableOpacity>

          <TouchableOpacity
            style={activeValue === 3 ?  styles.selectValueButton : styles.nonselectValueButton}
            onPress={() => {
              setActiveValue(3);
              onChangeNumber(3);
            }}
          ><Text style={styles.appButtonText}>3€</Text></TouchableOpacity>
          <TouchableOpacity
            style={activeValue === 5 ?  styles.selectValueButton : styles.nonselectValueButton}
            onPress={() => {
              setActiveValue(5);
              onChangeNumber(5);
            }}
          ><Text style={styles.appButtonText}>5€</Text></TouchableOpacity>
          <TouchableOpacity
            style={activeValue === 7 ?  styles.selectValueButton : styles.nonselectValueButton}
            onPress={() => {
              setActiveValue(7);
              onChangeNumber(7);
            }}
          ><Text style={styles.appButtonText}>7€</Text></TouchableOpacity>
          <TouchableOpacity
            style={activeValue === 10 ?  styles.selectValueButton : styles.nonselectValueButton}
            onPress={() => {
              setActiveValue(10);
              onChangeNumber(10);
            }}
          ><Text style={styles.appButtonText}>10€</Text></TouchableOpacity>
          <TextInput
            style={styles.input}
            onChangeText={(val) => {
              handleNumberChange(val);
            }}
            value={currency_value}
            placeholder="Custom amount"
            keyboardType="numeric"
          />
          <View style={styles.submitbutton}>
          <TouchableOpacity
            style={styles.selectSubmitButton }
            onPress={handleCustomSubmit}
          ><Text style={styles.appButtonText}>Submit</Text></TouchableOpacity>
          </View>
          {justSubmitted && <Text>Thank you!</Text>}
        </View>
      ) : (
        <View>
          <Text  style={styles.text}>Thank you for donating!</Text>
        </View>
      )}
    </View>
  );
}

const butStyle = {
  // backgroundColor: "white",
  height: 30,
  width: 30,  
}

const styles = StyleSheet.create({
  greenButton: {
    elevation: 8,
    backgroundColor: "green",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  redButton: {
    elevation: 8,
    backgroundColor: "#ed8e95",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  selectValueButton: {
    elevation: 8,
    backgroundColor: "#1130fa",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    width: 70,
    marginTop: 10
  },
  nonselectValueButton: {
    elevation: 8,
    backgroundColor: "#b3c8ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    width: 70,
    marginTop: 10
  },
  selectSubmitButton: {
    elevation: 8,
    backgroundColor: "#1130fa",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    width: 130,
    marginTop: 10
  },
  appButtonText: {
    fontSize: 25,
    color: "white",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  text1: {
    fontSize: 40,
    marginBottom: 100,
    fontWeight: "bold"
  },
  text: {
    fontSize: 40,
  },
  input: {
    marginTop: 20,
    fontSize: 30,
    height: 50,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
  },
  textbutton: {
    fontSize: 40,
    margin: 10,
    width: 400,

  },
  submitbutton: {
    fontSize: 40,
    margin: 10

  },
  submitbutton: {
    fontSize: 40,
    margin: 10

  },
});
